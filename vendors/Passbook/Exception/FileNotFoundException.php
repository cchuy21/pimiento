<?php

/*
 * This file is part of the Passbook package.
 *
 * (c) Eymen Gunay <eymen@egunay.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Passbook\Exception;

/**
 * Thrown when an error occurred in the component File
 *
 * @author Eymen Gunay <eymen@egunay.com>
 */
class FileNotFoundException extends FileException
{
    /**
     * Constructor.
     *
     * @param string $path The path to the file that was not found
     */
    public function __construct($path)
    { //SI EL ARCHIVO QUE ESTA BUSCANDO NO SE ENCUENTRA EN EL DIRECTORIO QUE SE HA ESPECIFICADO
        parent::__construct(sprintf('The file "%s" does not exist', $path));
    }
}
