<?php
class lenDriver extends driverBase {
    //función para iniciar sesión
    public static function inicio($len='ES') {
        $array=array();
        $len = strtoupper($len);
        //                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
        //                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
        //                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
        if($len=='ES'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['text1']='Un movimiento de Educando para apoyar la educación utilizando la tecnología, en agradecimiento a los maestros por su compromiso con sus alumnos.';
            $array['txtregistro']='¡Forma parte de esta comunidad y aprovecha todos los recursos que seleccionamos para los maestros mexicanos!<br><br>
                                    Este portal es una colección gratuita en línea de recursos de especialistas, maestros, directores, gerentes, profesionales de ONG´s y otros que trabajan en espacios educativos, que incluye:';
            $array['recursos1']='Talleres Vituales|Presentaciones de expertos';
            $array['recursos2']='Ebooks|Cursos con certificación';
            $array['patrocinadores']=array(1=>"cinepolis.png",2=>"cinepolis.png",3=>"cinepolis.png",4=>"cinepolis.png",5=>"cinepolis.png",6=>"cinepolis.png",7=>"cinepolis.png");
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['patrocinadorest'] = 'La realización de este proyecto no sería posible sin el apoyo de:';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
        
        } else if($len=='EN'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['text1']="Educando is creating a movement to thank educators for their commitment to their students, by supporting teachers' use of technology for education.";
            $array['txtregistro']='Be part of this community to enjoy all the resources that we have gathered for the teachers in Latin America!<br><br>
                                   This portal is a free online collection of resources from specialists, teachers, directors, managers, NGO professionals, and others working in educational spaces including:';
            $array['recursos1']='Virtual workshops|Specialist Presentations';
            $array['recursos2']='E-Books to download|Stress management materials';
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['patrocinadorest'] = 'The success of Thank A Teacher is possible thanks to the support of:';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";

        } else if($len=='PT'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['text1']='Um movimento da Educando para apoiar a educação usando a tecnologia, em agradecimento aos professores pelo seu compromisso com seus alunos.';
            $array['txtregistro']='Faça parte desta comunidade para aproveitar todos os recursos que selecionamos para os professores brasileiros!<br><br>
                                   Este portal é uma coleção gratuita e online de recursos de especialistas, professores, diretores, gerentes, profissionais de ONGs e outros que trabalham em áreas educacionais, incluindo:';
            $array['recursos1']='Oficinas virtuais|Apresentações de especialistas';
            $array['recursos2']='Ebooks e manuais|Cursos com certificação';
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['patrocinadores']=array(1=>"cinepolis.png",2=>"cinepolis.png",3=>"cinepolis.png",4=>"cinepolis.png",5=>"cinepolis.png",6=>"cinepolis.png",7=>"cinepolis.png");
            $array['patrocinadorest'] = 'A realização de Gratidão Professor não seria possível sem o apoio de:';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
        }
        return $array;
    }
    public static function campana($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){
            $array['texthash'] = 'Te invitamos a publicar un video en tus redes sociales agradeciendo a un maestro que destacó en tu vida con el hashtag';
            $array['parr1'] = 'Cada persona tiene un docente al cual ser agradecidos. Los maestros nos inspiran y nos preparan para ser miembros activos en la sociedad.';
            $array['parr2'] = 'Debido al Covid-19, 73 millones de mexicanos y brasileños no van a la escuela y sus maestros no pueden estar junto a sus alumnos para apoyarlos y motivarlos, cuando no se sienten seguros o cuando necesitan ayuda.';
            $array['parr3'] = 'Con esto en mente, Educando se ha unido con sus socios y nuevos aliados para apoyar a los educadores que buscan recursos para mejorar sus clases a distancia, así como instrumentos para mejorar sus habilidades de liderazgo y gestión en entornos educativos remotos. Del 05 de Octubre al 05 de Diciembre, en este portal, docentes, maestros, directores, gerentes, profesionales de las ONG´s y profesionistas que trabajan en espacios educativos, podrán escuchar a especialistas, descargar libros y manuales, participar en talleres virtuales y unirse a diversas comunidades de prácticas.';
            $array['parr4'] = '¡Todo lo que somos se lo debemos a los maestros y todo lo que hacemos en Educando es para los maestros! ¡Nuestra forma de agradecerles es apoyarlos en su crecimiento profesional! ';
            $array['parr5'] = '"Educando ha capacitado a educadores en México y Brasil durante 18 años en liderazgo, gestión escolar y educación STEM. Nuestros programas están presentes en 11,914 escuelas, a través de 16,093 educadores que impactan en conjunto a 6,374,046 de alumnos."';
            $array['parr6'] = 'Agradecemos a los maestros:';
            $array['parr7'] = 'Por no rendirse. Por aprender nuevas tecnologías y habilidades para continuar con su misión. ¡Por poner la educación de nuestros jóvenes y niños en primer lugar! Educando y sus socios quieren compartir su GRATITUD con ¡todos los maestros!';
            $array['logro'] = 'Logro';
            $array['patrocinio'] = 'Patrocinio';
            $array['apoyo'] = 'Apoyo';
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";

            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';

        } else if($len=='EN'){
            $array['texthash'] = 'We invite you to post a video on your social networks thanking a remarkable teacher in your life using the hashtag';
            $array['parr1'] = 'Each person has an educator that they are grateful for. Teachers inspire us and prepare us to be active members of society.';
            $array['parr2'] = 'Because of Covid-19, 73 million Mexicans and Brazilians are not going to school, and their teachers cannot be side by side with their students to support and motivate them when they do not feel confident or when they need help.';
            $array['parr3'] = 'With this in mind, Educando has teamed up with its current partners and new allies to support educators who are looking for resources to improve their distance learning classes, as well as instruments to improve their leadership and management skills in remote educational environments. From October 5th to December 5th, on the Thank a Teacher portal, teachers, directors, managers, NGO professionals, and others working in educational spaces will be able to listen to specialists, download e-books and manuals, participate in virtual workshops and join several communities of practice.';
            $array['parr4'] = 'Everything we are we owe to teachers, and everything we do at Educando is for teachers! Our way of thanking them is to support them in their professional growth.';
            $array['parr5'] = '"Educando has been training educators in leadership school management and STEM education for the past 18 years. Our programs are present in 11,914 schools, and through the 16,903 educator participants 6,374,046 students have been impacted."';
            $array['parr6'] = 'We are grateful to teachers:';
            $array['parr7'] = 'For not giving up. For learning new technologies and skills to continue their mission. For putting the education of our young people first! Educando and its partners want to share our GRATITUDE with all teachers!';
            $array['logro'] = 'Achievement';
            $array['patrocinio'] = 'Sponsorship';
            $array['apoyo'] = 'Support';
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';


        } else if($len=='PT'){

            $array['texthash'] = 'Convidamos você a postar um vídeo em suas redes sociais agradecendo um professor marcante em sua vida com a hashtag';
            
            $array['parr1'] = 'Cada pessoa tem um educador por quem ser grato: professores nos inspiram e nos preparam para sermos membros ativos da sociedade.';
            $array['parr2'] = 'Por causa do Covid-19, 73 milhões de mexicanos e brasileiros não estão indo à escola, e seus professores não podem estar lado a lado com seus alunos para apoiá-los e motivá-los, quando não se sentem confiantes ou quando necessitam de ajuda. ';
            $array['parr3'] = 'Pensando nisso, a Educando se uniu a seus parceiros e novos aliados para apoiar os educadores que estão buscando recursos para aprimorar as suas aulas a distância, bem como instrumentos para melhorar sua capacidade de liderança e gestão de ambientes educativos remotos. Do dia 5 de outubro ao 5 de dezembro, neste portal, professores, diretores, gestores, profissionais de ONGs, e demais atuantes em espaços educativos poderão ouvir especialistas, baixar e-books e manuais, participar em oficinas virtuais e integrar-se a diversas comunidades de práticas.';
            $array['parr4'] = 'Tudo o que somos devemos aos professores e tudo o que fazemos na Educando é pelos professores! A nossa maneira de agradecê-los é apoiá-los em seu crescimento profissional!';
            $array['parr5'] = '"A Educando tem capacitado educadores no México e no Brasil há 18 anos, em liderança, gestão escolar e educação STEM. Nossos programas estão presentes em 11.914 escolas, por meio de 16.093 educadores que impactam coletivamente a 6.374.046 alunos."';
            $array['parr6'] = 'Nós somos gratos aos professores:';
            $array['parr7'] = 'Por não desistirem. Por aprenderem novas tecnologias e habilidades para seguir com sua missão.Por colocarem a educação de nossos jovens e crianças em primeiro lugar! A Educando e os seus parceiros querem compartilhar sua GRATIDÃO a todos os professores! ';
            $array['logro'] = 'Realização';
            $array['patrocinio'] = 'Patrocínio';
            $array['apoyo'] = 'Apoio';
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
        }
        return $array;
    }
    public static function embajadores($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
        } else if($len=='EN'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
        } else if($len=='PT'){
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
        }
        return $array;
    }
    public static function aliados($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
            $array['manera'] = "¡La mejor manera de agradecer a un docente es ayudarlos a crecer!";
            $array['membresia1'] = "La membresía Aliados de Impacto de Educando, es una forma accesible y emocionante de brindar apoyo sostenible a nuestros programas de capacitación continua para nuestros docentes en América Latina.";
            $array['membresia2'] = "La membresía te permite unirte a una familia de seguidores que comprenden la importancia de comprometerse a largo plazo con la educación en América Latina.";
            $array['campeon'] = "¡Conviértete en un campeón de la educación de alta calidad para estudiantes de escuelas públicas en México y Brasil donando cualquier cantidad mensual!";
            $array['2002'] = "Impacto de Educando desde 2002";
            $array['lista'] = array("11,914 escuelas","16,093 educadores", "6,374,046 estudiantes");
            $array['tudonacion'] = "Tu donación apoyará los programas de Educando";
            $array['titulo1'] = 'LISTO MAE';
            $array['titulo2'] = 'STEM Brasil';
            $array['titulo3'] = 'STEM en México';
            $array['inspirando'] = 'Inspirando Maestros. Creando Líderes. Transformando Vidas.';
            $array['mensual'] = 'Dona mensualmente. Conviértete en un Aliado de Impacto.';
            $array['brntitulo'] = 'Beneficios de convertirte en un Aliado de Impacto';
            $array['bentxt1'] = 'Los Aliados de Impacto disfrutan de una variedad de beneficios diseñados para mantenerlos al tanto de todo lo que su inversión está haciendo posible.';
            $array['bentxt2'] = 'Actualizaciones de progreso trimestrales que contienen historias de un miembro de nuestra familia Educando.<br /> Fotos, cartas y videos de los estudiantes, maestros, escuelas y comunidades impactadas por tu continua generosidad.';
            $array['bentxt3'] = 'Maestros y estudiantes en toda América Latina, en las más de 11,914 escuelas que tienen programas de Educando, ¡quieren agradecer a todos nuestros Aliados de Impacto! Gracias a nuestros nuevos aliados:';

        } else if($len=='EN'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
            $array['manera'] = "The best way to thank a teacher is to help them grow!";
            $array['membresia1'] = "Educando’s Impact Partner Membership is an affordable, exciting way to provide sustained support of our teacher training programs in Latin America.";
            $array['membresia2'] = "Membership enables you to join a family of supporters who understand the importance of making a long-term commitment to education in Latin America.";
            $array['campeon'] = "Become a champion of high-quality education for public school students in Mexico and Brazil by donating any monthly amount!";
            $array['2002'] = "Educando’s Impact Since 2002";
            $array['lista'] = array("11,914 schools","16,093 educators", "6,374,046 students");
            $array['tudonacion'] = "Your donation will support Educando’s Programs";
            $array['titulo1'] = 'LISTO MAE';
            $array['titulo2'] = 'STEM Brasil';
            $array['titulo3'] = 'STEM in Mexico';
            $array['inspirando'] = 'Inspiring Teachers. Creating Leaders. Transforming Lives.';
            $array['mensual'] = 'Give Monthly. Become an Impact Partner.';
            $array['brntitulo'] = 'Benefits of Becoming an Impact Partner';
            $array['bentxt1'] = 'Impact Partners enjoy a range of benefits designed to keep your finger on the pulse of all that your investment is making possible.';
            $array['bentxt2'] = 'Quarterly progress updates containing stories from a member of our Educando family <br /> Photos, letters, and videos of the students, teachers, schools, and communities impacted by your continued generosity';
            $array['bentxt3'] = 'Teachers and students throughout Latin America at the more than 11,914 schools which have Educando programs, want to thank all our Impact Partners! Thank you to our newest supporters:';


        } else if($len=='PT'){
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
            $array['manera'] = "A melhor maneira para agradecer um professor é ajudá-lo a crescer!";
            $array['membresia1'] = "Aliados de Impacto é uma forma acessível e marcante para dar um apoio sustentável aos nossos programas de formação na América Latina.";
            $array['membresia2'] = "Ser Associado permite juntar-se a um grupo de apoiadores que entendem a importância de comprometer-se no longo prazo para educação na América Latina.  ";
            $array['campeon'] = "Seja um campeão de educação de alta qualidade para alunos da rede pública no México e Brasil por meio de doações mensais.  ";
            $array['2002'] = "O Impacto da Educando desde 2002";
            $array['lista'] = array("11,914 escolas","16,093 educadores", "6,374,046 alunos");
            $array['tudonacion'] = "Sua doação apoiará os programas da Educando";
            $array['titulo1'] = 'LISTO MAE';
            $array['titulo2'] = 'STEM Brasil';
            $array['titulo3'] = 'STEM en México';
            $array['inspirando'] = 'Inspirando Professores. Criando Líderes. Transformando Vidas.';
            $array['mensual'] = 'Doe Mensalmente. Seja um Aliado de Impacto.';
            $array['brntitulo'] = 'Benefícios ao se tornar um Aliado de Impacto';
            $array['bentxt1'] = 'Aliados de Impacto aproveitam de uma gama de benefícios desenhados para manter você a par do que o seu investimento está tornando possível.';
            $array['bentxt2'] = 'Atualizações trimestrais de progresso contendo histórias de um membro da família Educando<br /> Fotos, cartas e vídeos de alunos, professores, escolas e comunidades impactadas pela sua generosidade contínua';
            $array['bentxt3'] = 'Professores e alunos na America Latina nas mais de 11,914 escolas que têm programas da Educando, gostariam de agradecer todos os Associados de Impacto!  Agradecemos aos nossos novos apoiadores: ';


        }
        return $array;
    }
    public static function programa($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){
            $array['dias'] =array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
            $array['perfil'] = "Perfil";
            $array['oct'] = "Octubre";
            $array['nov'] = "Noviembre";
            $array['dec'] = "Diciembre";

        } else if($len=='EN'){
            $array['dias'] =array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
            $array['oct'] = "October";
            $array['nov'] = "November";
            $array['dec'] = "December";
        } else if($len=='PT'){
            $array['dias'] =array("Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado");
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            $array['oct'] = "Outubro";
            $array['nov'] = "Novembro";
            $array['dec'] = "Dezembro";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
        }
        return $array;
    }
    public static function empresa($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['sitio'] = "Sitio Web";
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
        } else if($len=='EN'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['sitio'] = "Website";
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
        } else if($len=='PT'){
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
            $array['sitio'] = "Website";

        }
        return $array;
    }
    public static function perfil($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){
            $array['paises'] = array("U.S.", "México", "Brazil","Guatemala","Honduras","El Salvador","Nicaragua","Costa Rica","Panama","Columbia","Ecuador","Venezuela","Peru","Bolivia","Chile","French Guaia","Paraguay","Argentina","Uruguay","Cuba","Dominican Republic", "Puerto Rico","Haiti","Canada","Other");

            $array['fnombre'] = "Nombre Completo";
            $array['ftelefono'] = "Número de Teléfono";
            $array['fempresa'] = "Nombre de la Empresa o Escuela";
            $array['fagradecer'] = "Mensaje de Agradecimiento";
            $array['fguardar'] = "Guardar";
            $array['fcerrar'] = "Cancelar";
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['activ'] = "Mi Actividad";
        } else if($len=='EN'){
            $array['paises'] = array("U.S.", "Mexico", "Brazil","Guatemala","Honduras","El Salvador","Nicaragua","Costa Rica","Panama","Columbia","Ecuador","Venezuela","Peru","Bolivia","Chile","French Guaia","Paraguay","Argentina","Uruguay","Cuba","Dominican Republic", "Puerto Rico","Haiti","Canada","Other");

            $array['fnombre'] = "Full Name";
            $array['ftelefono'] = "Phone Number";
            $array['fempresa'] = "Name of the Company or School";
            $array['fagradecer'] = "Message to Thank a Teacher";
            $array['fguardar'] = "Save";
            $array['fcerrar'] = "Cancel";
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['activ'] = "My Activity";

        } else if($len=='PT'){
            $array['paises'] = array("U.S.", "México", "Brazil","Guatemala","Honduras","El Salvador","Nicaragua","Costa Rica","Panama","Columbia","Ecuador","Venezuela","Peru","Bolivia","Chile","French Guaia","Paraguay","Argentina","Uruguay","Cuba","Dominican Republic", "Puerto Rico","Haiti","Canada","Other");
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            $array['fnombre'] = "Nome Completo";
            $array['ftelefono'] = "Número de telefone";
            $array['fempresa'] = "Nome da Empresa ou Escola";
            $array['fagradecer'] = "Mensagem de Agradecimento";
            $array['fguardar'] = "Guardar";
            $array['fcerrar'] = "Cancelar";

            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
            $array['activ'] = "Minha Atividade";

        }
        return $array;
    }
    public static function streaming($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){

            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['activ'] = "Mi Actividad";
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
        } else if($len=='EN'){
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['activ'] = "My Activity";
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
        } else if($len=='PT'){
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
            $array['activ'] = "Minha Atividade";

        }
        return $array;
    }
    public static function registro($len='ES') {
        $array=array();
        $len = strtoupper($len);

        if($len=='ES'){
            $array['paises'] = array("U.S.", "México", "Brazil","Guatemala","Honduras","El Salvador","Nicaragua","Costa Rica","Panama","Columbia","Ecuador","Venezuela","Peru","Bolivia","Chile","French Guaia","Paraguay","Argentina","Uruguay","Cuba","Dominican Republic", "Puerto Rico","Haiti","Canada","Other");
            $array['titulo1'] = "FORMATO DE REGISTRO";
            $array['titulo2'] = "GRACIAS A TI MAESTRO";
            $array['nombre'] ="NOMBRE Y APELLIDO";
            $array['contra'] ="CONTRASEÑA";
            $array['ccontra'] ="CONFIRMAR CONTRASEÑA";
            $array['telefono'] ="Número de Teléfono (10 dígitos)";
            $array['actividad'] ="ACTIVIDAD LABORAL";
            $array['ambiente'] ="AMBIENTE LABORAL";
            $array['nescuela'] ="NOMBRE DE LA ESCUELA O INSTITUCIÓN";
            $array['agradecer'] ="SI DESEA, UTILICE ESTE ESPACIO PARA AGRADECER A UN MAESTRO QUE CONOCE O HA INFLUIDO EN SU VIDA.";
            $array['veducador'] ="EDUCADOR";
            $array['votra'] ="OTRA PROFESIÓN";
            $array['vestudiante'] ="ESTUDIANTE";
            $array['votro'] ="OTRO";
            $array['vpublica'] ="ESCUELA PÚBLICA";
            $array['vprivada'] ="ESCUELA PRIVADA";
            $array['vong'] ="ONG";
            $array['vgob'] ="OFICINA DE GOBIERNO";
            $array['vemp'] ="EMPRESA PRIVADA";
            $array['voblig'] ="*Obligatorio";
            $array['guardar'] ="GUARDAR";
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Cerrar Sesión";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }else{
                $array['menu']=array("INICIO"=>'/main/index/es',"CAMPAÑA"=>'/campana/index/es',"PROGRAMA"=>'/programa/index/es',"EMBAJADORES"=>"/embajadores/index/es","ALIADOS DE IMPACTO"=>'/aliados/index/es');
            }
            $array['donart1'] = 'Ayúdanos a proporcionar mayor calidad educativa a las escuelas en Latinoamérica y conviértete en un Aliado de Impacto.';
            $array['donart2'] = '¡GRACIAS POR AYUDARNOS!';
            $array['ligadonar'] = 'https://alwayson.recaudia.com/educando';
            $array['activ'] = "Mi Actividad";
        } else if($len=='EN'){
            $array['paises'] = array("U.S.", "Mexico", "Brazil","Guatemala","Honduras","El Salvador","Nicaragua","Costa Rica","Panama","Columbia","Ecuador","Venezuela","Peru","Bolivia","Chile","French Guaia","Paraguay","Argentina","Uruguay","Cuba","Dominican Republic", "Puerto Rico","Haiti","Canada","Other");
            $array['titulo1'] = "THANK A TEACHER";
            $array['titulo2'] = "REGISTRATION FORM";
            $array['nombre'] ="FULL NAME";
            $array['contra'] ="PASSWORD";
            $array['ccontra'] ="CONFIRM PASSWORD";
            $array['telefono'] ="PHONE NUMBER";
            $array['actividad'] ="YOUR PROFESSION";
            $array['ambiente'] ="WORKPLACE";
            $array['nescuela'] ="NAME OF THE INSTITUTION OR SCHOOL";
            $array['agradecer'] ="IF YOU'D LIKE PLEASE USE THIS SPACE TO THANK A TEACHER YOU KNOW OR THAT HAS INFLUENCED YOUR LIFE.";
            $array['veducador'] ="EDUCATOR";
            $array['votra'] ="OTHER PROFESSIONAL";
            $array['vestudiante'] ="STUDENT";
            $array['votro'] ="OTHER";
            $array['vpublica'] ="PUBLIC SCHOOL";
            $array['vprivada'] ="PRIVATE SCHOOL";
            $array['vong'] ="NGO";
            $array['vgob'] ="GOVERNMENT OFFICE";
            $array['vemp'] ="PRIVATE COMPANY";
            $array['voblig'] ="*Required";
            $array['guardar'] ="SAVE";
            $array['perfil'] = "Profile";
            $array['cerra'] = "Log Out";
            if( authDriver::isLoggedin() ) {
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }else{
                $array['menu']=array("HOME"=>'/main/index/en',"CAMPAIGN"=>'/campana/index/en',"PROGRAM"=>'/programa/index/en',"AMBASSADORS"=>"/embajadores/index/en","IMPACT PARTNERS"=>'/aliados/index/en');
            }
            $array['donart1'] = 'Help us provide higher quality education to schools in Latin America. Become an Impact Partner';
            $array['donart2'] = 'Thanks for your support!';
            $array['ligadonar'] = 'https://fundraise.educando.org/give/299059/#!/donation/checkout';
            $array['activ'] = "My Activity";

        } else if($len=='PT'){
            $array['paises'] = array("U.S.", "Mexico", "Brazil","Guatemala","Honduras","El Salvador","Nicaragua","Costa Rica","Panama","Columbia","Ecuador","Venezuela","Peru","Bolivia","Chile","French Guaia","Paraguay","Argentina","Uruguay","Cuba","Dominican Republic", "Puerto Rico","Haiti","Canada","Other");
            $array['perfil'] = "Perfil";
            $array['cerra'] = "Fechar";
            $array['titulo1'] = "FORMULÁRIO DE REGISTRO";
            $array['titulo2'] = "GRATID&Atilde;O PROFESSOR";
            $array['nombre'] ="NOME COMPLETO";
            $array['contra'] ="SENHA";
            $array['ccontra'] ="CONFIRMAR SENHA";
            $array['telefono'] ="NÚMERO DE TELEFONE (COM DDD)";
            $array['actividad'] ="PROFISS&Atilde;O";
            $array['ambiente'] ="LUGAR DE TRABALHO";
            $array['nescuela'] ="NOME DA EMPRESA OU ESCOLA";
            $array['agradecer'] ="SE VOC&Ecirc; QUISER, DEIXE AQUI UMA MENSAGEM DE AGRADECIMENTO A UM PROFESSOR QUE INFLUENCIOU A SUA VIDA!";
            $array['veducador'] ="EDUCADOR";
            $array['votra'] ="OUTRO PROFISSIONAL";
            $array['vestudiante'] ="ESTUDANTE";
            $array['votro'] ="OUTRO";
            $array['vpublica'] ="ESCOLA PÚBLICA";
            $array['vprivada'] ="ESCUELA PRIVADA";
            $array['vong'] ="ONG";
            $array['vgob'] ="ÓRG&Atilde;O DE GOVERNO";
            $array['vemp'] ="EMPRESA PRIVADA";
            $array['voblig'] ="*Obrigatório";
            $array['guardar'] ="GUARDAR";

            if( authDriver::isLoggedin() ) {
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }else{
                $array['menu']=array("INÍCIO"=>'/main/index/pt',"A CAMPANHA"=>'/campana/index/pt',"PROGRAMAÇÃO"=>'/programa/index/pt',"EMBAIXADORES"=>"/embajadores/index/pt","ALIADOS DE IMPACTO"=>'/aliados/index/pt');
            }
            $array['donart1'] = 'Ajude-nos a proporcionar maior qualidade educativa às escolas latinoamericanas, torne-se um Aliado de Impacto.';
            $array['donart2'] = 'Obrigado pelo seu apoio!';
            $array['ligadonar'] = 'https://paybox.doare.org/br/paybox?sourceURL=https:%2F%2Fdoare.org%2Feducando-by-worldfund&lang=br&values=30,70,150,300,500&currency=BRL&showIncludeCosts=1&orgId=dbd60262-035e-11eb-9b5e-060a6e179d39&subscribe=1&subscriptionAmount=&showSubscription=1&referer=https:%2F%2Fdoare.org%2Feducando-by-worldfund';
            $array['activ'] = "Minha Atividade";

        }
        return $array;
    }

}