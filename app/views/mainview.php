<?php

class mainView extends viewBase {
	
	public function index() {
		templateDriver::setData("Lan", "es");
        templateDriver::render("main.header");
        
        templateDriver::setData("Lan", "es");
        templateDriver::render("main.main");
        
        templateDriver::setData("Lan", "es");
        templateDriver::render("main.footer");
	}
	public function main($var) {
		templateDriver::setData("Lan", $var);
        templateDriver::render("main.header");
    }
	
}