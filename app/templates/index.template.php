<?php
//session_start();
$actual_link = explode("/",$_SERVER[REQUEST_URI]);
?>
<!DOCTYPE HTML>
<html> <!--archivo principal del servicio, se mandan a llamar los estilos css y js-->
    <head><meta charset="gb18030">
        
        <title><?php echo strtoupper($actual_link[1]); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" type="image/png" href="/static/images/favicon.jpg">

        <link rel="stylesheet" href="/static/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/css/bootstrap-theme.min.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <?php
                    echo '<link href="/static/css/style.css" rel="stylesheet">';

          if($actual_link[1]=="" || $actual_link[1] == "main"){
            echo '<link href="/static/css/main.css" rel="stylesheet">';
          }else{
            echo '<link href="/static/css/'.$actual_link[1].'.css" rel="stylesheet">';
          }
        ?>      
    </head>
    <body>
        
        <?php
          $actual_link = explode("/",$_SERVER[REQUEST_URI]);
          templateDriver::content();
        ?>

  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <script src="/static/js/jssor.slider.min.js"></script>
    <!-- Popper.JS -->
    <!-- Bootstrap JS -->
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script>
      $(document).ready(function () {
        if($(".vdeo").length){
          if($(window).width() < 768){
          $('.vdeo source').attr('src', "/static/videos/carromobile.mp4");
          $(".vdeo").addClass("vdeomob");

          }else{
            $('.vdeo source').attr('src', "/static/videos/carrodesktop.mp4");
            $(".vdeo").addClass("vdeodesk");

          }
          $(".vdeo")[0].load();
        }
      });
    </script>
  </html>