<style>
    .navbar-right a:hover{
        color: #00c1ff!important;
    }
    .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover{
        background-color:transparent!important;
        background-image:none!important;
        color: #00c1ff!important;

    }
</style>
<img src="/static/images/PUBLICO/KSW2_PUNTOSDERECHA.png" class="izqu" style="position: fixed; right: 0px; top: 20%;" />
<img src="/static/images/PUBLICO/KSW2_PUNTOSIZQUIERDA.png" class="dere" style="position: fixed; left: 0px; top: 50%;" />
<header style='z-index: 9999999; position: absolute; width: 100%;'>
    <nav class="navbar navbar-default">
        <section class="container-fluid">
            
            <section class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img class="logo" src="/static/images/PUBLICO/KSW2_LOGOARNESES.png" /></a>
            </section>
            <section class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href='/'>Inicio</a></li>
                    <li><a href='/mision'>Misión</a></li>
                    <li><a href='/vision'>Visión</a></li>
                    <li><a href='/politica'>Política de calidad</a></li>
                    <li><a href='/vacantes'>Vacantes</a></li>
                    <li><a href='/proveedores'><button class="btn btn-proveedores">Proveedores</button></a></li>
                </ul>
            </section>
        </section>
    </nav>
</header>
<section>
        <center>
            <video width="100%" height="auto" autoplay="true" muted="muted" playsinline="" style="z-index: -1;">
                <source src="/static/videos/carro.mp4" type="video/mp4">
            </video>
        </center>
</section>
<footer class="row" style='position: fixed; bottom: 0;'>
    <section class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
        <section class="col-sm-1" style='border: 1px solid white; margin-top: 10px;'></section>
        <section class="col-xs-12 col-sm-11"> "DANDO SEGUIMIENTO A LA POLÍTICA DE LA COMPAÑÍA CONTRA  LA CORRUPCIÓN Y LAS MALAS PRACTICAS, CUALQUIER SITUACIÓN DENUNCIARLA".<br />AL TELÉFONO 910 06 00 EXT. 1236 O A LA CUENTA DE CORREO <a href="mailto:denuncia@ksmex.com.mx" style='color:white!important'>denuncia@ksmex.com.mx</a></section>       
    </section>
    <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <section class="col-xs-12 col-sm-10">ⓒ Copyright 2019 Todos Los Derechos Reservados.</section>
        <section class="col-sm-2" style='border: 1px solid white; margin-top: 10px;'></section>
    </section>
</footer>